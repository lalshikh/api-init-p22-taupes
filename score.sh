#!/bin/bash

function augmentation_score ()
{	
	score="$(( $1 + $2 ))"
}

function diminution_score ()
{	
	score="$(( $1 - $2 ))"
}

